require 'pagy'
include Pagy::Backend
include Pagy::Frontend

class StationsController < ApplicationController
    def index
        @pagy, @stations = pagy(Station.all)
    end
end
