import { Controller } from "stimulus";
import { useMutation } from 'stimulus-use'
import { Map, Marker, LngLatBounds} from "maplibre-gl";

export default class extends Controller {
  static targets = ["station"];

  makeMarker(el) {
    const lng = el.getAttribute('data-lon');
    const lat = el.getAttribute('data-lat');
    const name = el.getAttribute('data-name');
    const marker = new Marker()
      .setLngLat({lng, lat})
      .addTo(this.map);
    this.markers.push(marker);
  }

  reloadMarkers() {
    this.markers.forEach(marker => marker.remove());
    this.markers = [];
    this.stationTargets.forEach(el => this.makeMarker(el))
    var bounds = new LngLatBounds();
    this.markers.forEach(marker => { bounds.extend(marker._lngLat) });
    this.map.fitBounds(bounds);
  }

  connect() {
    this.markers = [];
    useMutation(this, { childList: true, subtree: true })

    this.map = new Map({
      container: 'map',
      style: 'https://api.maptiler.com/maps/basic/style.json?key=rF1iMNeNc3Eh3ES7Ke8H',
      center: [0, 0],
      zoom: 1
    });
    this.reloadMarkers();
  }

  mutate() {
    this.reloadMarkers();
  }
}
