class CreateStations < ActiveRecord::Migration[6.1]
  def change
    create_table :stations do |t|
      t.string :name
      t.string :uic
      t.float :lon
      t.float :lat

      t.timestamps
    end
  end
end
