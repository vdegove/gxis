require 'csv'

desc "Populates the station table with data from https://github.com/trainline-eu/stations/"
task import_stations: [:environment] do
    CSV.foreach('data/stations_small.csv', headers: true, col_sep: ';') do |row|
        Station.create!(row.to_hash)
    end
end
